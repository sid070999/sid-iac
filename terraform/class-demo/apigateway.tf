

# rest api # resource "aws_api_gateway_rest_api"
resource "aws_api_gateway_rest_api" "api_gateway" {
  name="deo-api-gateway"
  description = "demo api gateway"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}



# resource end point
resource "aws_api_gateway_resource" "api_gateway_resource" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  parent_id = aws_api_gateway_rest_api.api_gateway.root_resource_id
  path_part = "hello"
  
}

# method
resource "aws_api_gateway_method" "api_gateway_method" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  resource_id = aws_api_gateway_resource.api_gateway_resource.id
  http_method = "GET"
  authorization = "NONE"
}

# integration ==> lambda
resource "aws_api_gateway_integration" "api_gateway_lambda_integration" {
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  resource_id = aws_api_gateway_resource.api_gateway_resource.id
  http_method = aws_api_gateway_method.api_gateway_method.http_method
  integration_http_method = "GET"
  type = "AWS_PROXY"
  uri = aws_lambda_function.example_fnc.invoke_arn
}

# deployment
resource "aws_api_gateway_deployment" "api_gateway_deployment" {
  depends_on = [aws_api_gateway_integration.api_gateway_lambda_integration]
  rest_api_id = aws_api_gateway_rest_api.api_gateway.id
  stage_name = "uat"
}

# output the url
output "api_gateway_url" {
  value = aws_api_gateway_deployment.api_gateway_deployment.invoke_url
}