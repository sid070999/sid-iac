
variable "env" {
  type = string
  default = "dev"
}

resource "aws_vpc" "demo_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "demo-vpc-${var.env}"
  }
}


resource "aws_security_group" "security_group" {
  name_prefix = "demo-secgrp"
  vpc_id = aws_vpc.demo_vpc.id

  ingress {
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_subnet" "demo_subnet" {
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.demo_vpc.id
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true
  
  tags = {
    Name = "demo_subnet"
  }
}

output "return_vpc_id" {
  value = aws_vpc.demo_vpc.id
}

output "return_subnet_id" {
  value = aws_subnet.demo_subnet.id
}

output "return_secgrp_id" {
  value = aws_security_group.security_group.id
}
