
resource "aws_internet_gateway" "igw" {
  vpc_id = module.my_network.return_vpc_id

  tags = {
    Name = "${module.my_network.return_vpc_id}-internet-gateway"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = module.my_network.return_vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "public"
  }
}

resource "aws_route_table_association" "route_table_subnet_association" {
  subnet_id      = module.my_network.return_subnet_id
  route_table_id = aws_route_table.public_route_table.id
}

## Output
output "aws_internet_gateway_id" {
  value       = aws_internet_gateway.igw.id
  description = "Internet gateway id."
}

output "igw_aws_account" {
  value       = aws_internet_gateway.igw.owner_id
  description = "AWS Account id to which internet gateway is associated."
}

